﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProBuilder2.Common;
using UnityEngine;
using UnityEngine.Events;

public class fov : MonoBehaviour
{
    public GameObject enemy;
    public GameObject fovPlayer;
    public float maxAngle = 45;
    public float maxRadius;

    public List<GameObject> enemiesDetect = new List<GameObject>();
    public List<GameObject> enemiesFoV = new List<GameObject>();

    public EnemyUnityEvent onEnemyDetected; // desgina a accao de deteccao do inimigo no fov
    public UnityEvent onEnemyLost;  //inimigos fora do fov

    private void Update()
    {
        enemiesFoV.Clear();

        foreach (var enemy in enemiesDetect)
        {
            var enemyPosition = enemy.transform.position;
            var myPosition = transform.position;

            var myForward = transform.forward; // direction car 

            Vector3 directionToEnemy = (enemyPosition - myPosition).normalized;

            var angle = Vector3.Angle(myForward, directionToEnemy);

            if (angle <= maxAngle)
            {
                enemiesFoV.Add(enemy);
            }
        }
        //sort dos inimigos

        enemiesFoV.Sort(SortEnemies);

        if (enemiesFoV.Count > 0)
        {
            onEnemyDetected.Invoke(enemiesFoV[0]); // add enemy
        }
        else
        {
            onEnemyLost.Invoke(); //se perder o inimigos dentro do fov
        }

    }
    int SortEnemies(GameObject a, GameObject b)
    {
        float distanceToA = Vector3.Distance(transform.position, a.transform.position);
        float distanceToB = Vector3.Distance(transform.position, b.transform.position);

        return Math.Sign(distanceToA-distanceToB); // faz a ordenacao com base na distance entre estes 2 valores 
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Vehicle"))
        {
            if (!enemiesDetect.Contains(other.gameObject))
            {
                enemiesDetect.Add(other.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Vehicle"))
        {
            enemiesDetect.Remove(other.gameObject); // verifica e remove o object
        }
    }
}

[System.Serializable]
public class EnemyUnityEvent : UnityEvent<GameObject>
{
    
}
