﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static bool isinitializemusic;

    // Start is called before the first frame update
    void Start()
    {
        if (isinitializemusic)
        {
            Destroy(gameObject);
        }
        else
        {
            isinitializemusic = true;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
