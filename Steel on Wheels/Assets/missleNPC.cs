﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class missleNPC : MonoBehaviour
{
    private GameObject rocketTarget;
    public Rigidbody RocketRigidbody;

    public float turnSpeed = 1f;
    public float rocketFlySpeed = 50f;

    private Transform rocketLocalTrans;

 
    
    private HP _enemyHp;
    
    void Start()
    {
        rocketLocalTrans = GetComponent<Transform>();
        _enemyHp = GameObject.FindGameObjectWithTag("Vehicle").GetComponent<HP>();
        rocketTarget = GameObject.FindGameObjectWithTag("Vehicle");
      
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rocketTarget != null)
        {
            if (!RocketRigidbody)
            {
                return;
            }

            RocketRigidbody.velocity = rocketLocalTrans.forward * rocketFlySpeed; // velocidade do rocket

            var rocketTargetRot = Quaternion.LookRotation(rocketTarget.transform.position - rocketLocalTrans.position); //rotation in order to face target

            RocketRigidbody.MoveRotation(Quaternion.RotateTowards(rocketLocalTrans.rotation, rocketTargetRot, turnSpeed));

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Vehicle"))
        {
            _enemyHp.RemoveHp(30);
            Destroy(gameObject);
            Debug.Log("Missil OK");
        }
    }
}
