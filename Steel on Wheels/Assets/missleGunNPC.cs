﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class missleGunNPC : MonoBehaviour
{
    private GameObject rocketTarget;
    public GameObject rocketPrefab; 
    public Transform spawnMisslePoint; //local de instancicao da bala

    private bool firing;
    private float last_Fire_Time = -1f;

    public float frequency = 5f;
    
    private int ammo = 5;
   
    private GameObject AudioManager;

    private FMODUnity.StudioEventEmitter[] emitters;

    void Start()
    {
        rocketTarget = GameObject.FindGameObjectWithTag("Vehicle");
        AudioManager = GameObject.FindGameObjectWithTag("AudioManager");
        emitters = GetComponents<FMODUnity.StudioEventEmitter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ammo <= 0 && Time.time > last_Fire_Time + 30f)
        {
            ammo = 5;
            firing = true;
            //Debug.Log("Reloaded");
        } else if (rocketTarget != null && ammo > 0)
        {
            firing = true;
            
            if (firing)
            {
                if (Time.time > last_Fire_Time + frequency) // tempo entre tiros
                {
                    GameObject bullet = Instantiate(rocketPrefab, spawnMisslePoint.position, Quaternion.identity); // instanciacao da bala
                 
                    ammo--;
                    last_Fire_Time = Time.time; //forma que verificar a ultima vez que foi dispardo
                    emitters[1].Play();
                }
                if (ammo == 0)
                {
                    firing = false;
                    //Debug.Log("Sem Ammo");
                }
            }
        }
    }

    public void SetMissleTarget(GameObject target) // verfica se o carro esta dentro do fov 
    {
        rocketTarget = target; //adciciona o carro como um posssivel target
    }

    public void ClearMissleTarget()
    {
        rocketTarget = null; // se nao tiver dentro limpa a lista
    }
}
