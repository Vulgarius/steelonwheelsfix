﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrive : MonoBehaviour {

    public GameObject Player; //Body do objeto que a câmera segue
    public Drive RR; //Script Drive do veículo a ser seguido
    private GameObject cameralookAt, cameraPos;
    private float speed = 0;
    private float defaltFOV = 0, desiredFOV = 0;
    [Range (0, 50)] public float smothTime = 8;
    private GameObject AudioManager;

    private void Awake()
    {
        AudioManager = GameObject.FindGameObjectWithTag("AudioManager");
    }

    private void Start () {
        //Player = GameObject.FindGameObjectWithTag ("Vehicle");
        RR = Player.GetComponent<Drive> ();
        cameralookAt = Player.transform.Find ("camera lookAt").gameObject;
        cameraPos = Player.transform.Find ("camera constraint").gameObject;

        defaltFOV = Camera.main.fieldOfView;
        desiredFOV = defaltFOV + 15;
        GetComponent<FMODUnity.StudioEventEmitter>().Play();
    }
    
    private void FixedUpdate () {
        follow ();
    }
    
    private void follow () {
        speed = RR._velocidadeInstantanea / smothTime;
        gameObject.transform.position = Vector3.Lerp (transform.position, cameraPos.transform.position ,  Time.deltaTime * speed);
        gameObject.transform.LookAt (cameralookAt.gameObject.transform.position);
    }
}