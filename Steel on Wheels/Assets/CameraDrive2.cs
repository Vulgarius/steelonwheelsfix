﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrive2 : MonoBehaviour {

    private GameObject Player;
    private Carro_TesteIa RR;
    private GameObject cameralookAt, cameraPos;
    private float speed = 0;
    private float defaltFOV = 0, desiredFOV = 0;
    [Range (0, 50)] public float smothTime = 8;

    private void Start () {
        Player = GameObject.FindGameObjectWithTag ("Vehicle2");
        RR = Player.GetComponent<Carro_TesteIa> ();
        cameralookAt = Player.transform.Find ("camera lookAt 2").gameObject;
        cameraPos = Player.transform.Find ("camera constraint 2").gameObject;

        defaltFOV = Camera.main.fieldOfView;
        desiredFOV = defaltFOV + 15;
    }

    private void FixedUpdate () {
        follow ();
        //boostFOV ();

    }
    private void follow () {
        speed = RR._velocidadeInstantanea / smothTime;
        gameObject.transform.position = Vector3.Lerp (transform.position, cameraPos.transform.position ,  Time.deltaTime * speed);
        gameObject.transform.LookAt (cameralookAt.gameObject.transform.position);
    }
    
    // private void boostFOV () {
    //
    //     if (RR.nitrusFlag)
    //         Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, desiredFOV, Time.deltaTime * 5);
    //     else
    //         Camera.main.fieldOfView = Mathf.Lerp (Camera.main.fieldOfView, defaltFOV, Time.deltaTime * 5);
    //
    // }

}