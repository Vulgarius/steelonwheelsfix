﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveColliders : MonoBehaviour
{
    private BoxCollider _boxCollider;
    private SphereCollider _sphereCollider, _sphere;
    private GameObject _esfera;
    
    void Start()
    {
        _boxCollider = GetComponent<BoxCollider>();
        _sphereCollider = GetComponent<SphereCollider>();
        _esfera = GameObject.FindGameObjectWithTag("tagEsfera");
        _sphere = _esfera.GetComponent<SphereCollider>();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("combat"))
        {
            _boxCollider.enabled = true;
            _sphereCollider.enabled = true;
            _sphere.enabled = true;
        }
    }
}
