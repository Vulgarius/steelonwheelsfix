﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;

public class NavMeshNPC : MonoBehaviour
{
   
    //public GameObject[] wps; //conjunto de waysPoints
    private UnityEngine.AI.NavMeshAgent nissanAgent; //link to NPC

    public Transform _destination;
    
    //int currentWaypoint = 0;
    
    void Start()
    {
      
       nissanAgent = this.GetComponent<UnityEngine.AI.NavMeshAgent>(); //assigna o navmesh
        
    }

    void Update()
    {
        FinalDestination();
    }

    public void FinalDestination() // modo de asignar o destino final do NPC
    { 
        //nissanAgent.SetDestination(wps[0].transform.position); //o NPC vai ser deslocal em direcao a posicao final
        Vector3 targetVector = _destination.transform.position;
        nissanAgent.SetDestination(targetVector);
        
    }

    public void InsideArena() //se o jogador entrar na Arena
    {
        
    }

    private void LateUpdate()
    {
       
    }
}
