﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomNosPostes : MonoBehaviour
{
    private Collider teste;
    
    private GameObject AudioManager;
    // Start is called before the first frame update
    void Start()
    {
        AudioManager = GameObject.FindGameObjectWithTag("AudioManager");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Vehicle"))
        {
            GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
}
    