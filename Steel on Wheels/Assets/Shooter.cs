﻿using System.Collections;
using System.Collections.Generic;
using CodingJar;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    private GameObject currentTarget;
    public GameObject bulletPrefab; 
    public Transform spawnPoint; //local de instancicao da bala
    
    private float cone_Angle = 1.5f;

    private bool firing;
    private float last_Fire_Time = -1f;

    public float frequency = 0.2f;

    private int ammo = 30;
    
    private GameObject AudioManager;
    
    private FMODUnity.StudioEventEmitter[] emitters;

    void Start()
    {
        currentTarget = GameObject.FindGameObjectWithTag("Vehicle");
        AudioManager = GameObject.FindGameObjectWithTag("AudioManager");
        emitters = GetComponents<FMODUnity.StudioEventEmitter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ammo <= 0 && Time.time > last_Fire_Time + 10f)
        {
            ammo = 30;
            firing = true;
            //Debug.Log("Reloaded");
        } else if (currentTarget != null && ammo > 0)
        {
            firing = true;

            if (firing)
            {
                if (Time.time > last_Fire_Time + frequency) // tempo entre tiros
                {
                    GameObject bullet = Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation); // instanciacao da bala
                    ammo--;
                    last_Fire_Time = Time.time; //forma que verificar a ultima vez que foi dispardo
                    emitters[0].Play();
                }
                if (ammo == 0)
                {
                    firing = false;
                    //Debug.Log("Sem Ammo");
                }
            }
        }
    }

    public void SetTarget(GameObject target) // verfica se o carro esta dentro do fov 
    {
        currentTarget = target; //adciciona o carro como um posssivel target
    }

    public void ClearTarget()
    {
        currentTarget = null; // se nao tiver dentro limpa a lista
    }
}
