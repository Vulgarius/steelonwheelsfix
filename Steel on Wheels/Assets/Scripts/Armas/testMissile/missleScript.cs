﻿using System;
using UnityEngine;


    public class missleScript : MonoBehaviour
    {

        private Transform target; 
        
        private Vector3 _direction;
        
        [Range (1, 50)] public int missile_damage; //Dano causado pela bala

        public float speedMissile = 20;
        public float rotationSpeed = 5;
        

        void Start()
        {
            
            target = GameObject.FindGameObjectWithTag("Vehicle2").transform;
        }
        void Update()
        {
            
            //movevemt do missil
            transform.Translate(Vector3.forward * (speedMissile * Time.deltaTime)); 
            
            //rotation missile consoante o target

            if (target != null ) 
            {
                _direction = target.position - transform.position; 

                _direction = _direction.normalized;
                var rot = Quaternion.LookRotation((_direction));
                transform.rotation = Quaternion.Slerp(transform.rotation , rot, rotationSpeed * Time.deltaTime);
            }
           
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Vehicle2" || other.gameObject.tag == "Target") //Testa se a bala atingiu um objeto com a tag "Vehicle"
            {
              //  other.gameObject.GetComponent<Vehicle>().Damage(2, missile_damage, speedMissile); //Veículo sendo atingido
             // Destroy(this.gameObject); //Destrói a missile sob impacto
            }
        }
           
    }

