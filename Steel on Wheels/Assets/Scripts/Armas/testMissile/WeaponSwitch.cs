﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSwitch : MonoBehaviour
{
    public int selectedWeapon = 0;
    
    [SerializeField]
    private Text _bulletActive, _missleActive, _gun, _missile;

    private bool _combatOk;
    
    void Start()
    {
        
    }

   

    void Update()
    {
        if (_combatOk)
        {
            SelectedWeapon();
            
            SetActiveText();
            int previousSelectWeapon = selectedWeapon;
        
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                if (selectedWeapon >= transform.childCount - 1)
                    selectedWeapon = 0;
                else
                    selectedWeapon++;
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                if (selectedWeapon <= 0)
                    selectedWeapon = transform.childCount - 1;

                else
                    selectedWeapon--;
            }

            if (previousSelectWeapon != selectedWeapon)
            {
                SelectedWeapon();
            } 
        }
    }
    
    private void SelectedWeapon()
    {
        int i = 0;

        foreach (Transform weapon in transform)
        {
            if (i == selectedWeapon) // primeiramente e selecionada a primeira arma por defeito
            {
                weapon.gameObject.SetActive(true);
            }
            else
            {
                weapon.gameObject.SetActive(false); // desativa a que nao esta selecionada 
            }
            i++;
        }
    }
    
    public void SetActiveText() // ui numero de balas
    {
        if (selectedWeapon == 0)
        {
            _bulletActive.text = ">";
            _missleActive.text = " ";
        }else if (selectedWeapon == 1)
        {
            _bulletActive.text = " ";
            _missleActive.text = ">";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("combat"))
        {
            _bulletActive.GetComponent<Text>().enabled = true;
            _gun.GetComponent<Text>().enabled = true;
            _missile.GetComponent<Text>().enabled = true;
            _missleActive.GetComponent<Text>().enabled = true;

            _combatOk = true;
            //SelectedWeapon();
        }
    }
}
