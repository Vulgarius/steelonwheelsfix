﻿using UnityEngine;
using UnityEngine.UI;

namespace Armas.testMissile
{
    public class SimpleShoot : MonoBehaviour
    {
        public GameObject bulletPrefab; // prefab da bala
        public Transform spawnPoint; //instanciacao da bala

        [Header("Controls")]
        public KeyCode keyFire; //Botão usado para atirar
    
        [SerializeField]
        private Text _bulletText;

        //private AudioSource audioManager;
        private float last_Fire_Time = -1f;

        public float frequency = 0.2f;

        private int ammo = 30;

        private GameObject AudioManager;

        void Start()
        {
            SetBulletText();
            AudioManager = GameObject.FindGameObjectWithTag("AudioManager");
        }

    
        void Update()
        {
            SetBulletText();
            if (ammo <= 0 && Time.time > last_Fire_Time + 10f)
            {
                ammo = 30;
                Debug.Log("Reloaded");
            }
            if (Input.GetKey(keyFire) )
            {
                if (ammo > 0)
                {
                    FireGun();
                }
            }
        }
    
        void FireGun()  // tiro da arma
        {
            if (Time.time > last_Fire_Time + frequency) // tempo entre tiros
            {
                GameObject bullet = Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation); // instanciacao da bala
                ammo--;
                last_Fire_Time = Time.time; //forma que verificar a ultima vez que foi dispardo
                GetComponent<FMODUnity.StudioEventEmitter>().Play();
            }
            if (ammo == 0)
            {
                Debug.Log("Sem Ammo");
            }
        }
    
        public void SetBulletText() // ui numero de balas
        {
            if (_bulletText)  // se o texto existe
            {
                if (ammo == 0)
                {
                    _bulletText.text = "Reloading...";
                }else
                    _bulletText.text = "Bullets: " + ammo; // mostra na UI
            }
        }
    }
}

