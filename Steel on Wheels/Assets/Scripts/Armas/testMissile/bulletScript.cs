﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    [Range (1, 50)] public int bullet_damage; //Dano causado pela bala
    
    [Header("Parameters")]     // parametros com range
    [Range(5f, 500.0f)] public float speedBullet; // velocidade da bala q
   

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * (speedBullet * Time.deltaTime)); 
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Vehicle" || other.gameObject.tag == "Target") //Testa se a bala atingiu um objeto com a tag "Vehicle"
        {
           // collision.gameObject.GetComponent<Vehicle>().Damage(2, bullet_damage, speedBullet); //Veículo sendo atingido
            Destroy(this.gameObject); //Destrói a bala sob impacto
        }
       
      
    }
}


