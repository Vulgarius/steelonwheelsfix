﻿using UnityEngine;
using UnityEngine.UI;

namespace Armas.testMissile
{
    public class missleGun : MonoBehaviour
    {
        public GameObject missilPrefab; // prefab da bala
        public Transform spawnPoint; //instanciacao da bala
        private GameObject AudioManager;
        [Header("Controls")]
        public KeyCode keyFire; //Botão usado para atirar
    
        [SerializeField]
        private Text _missilText;

        //private AudioSource audioManager;
        private float last_Fire_Time = -1f;

        public float frequency = 5f;

        private int ammo = 5;

        void Start()
        {
            SetMissilText();
            AudioManager = GameObject.FindGameObjectWithTag("AudioManager");
        }

    
        void Update()
        {
            SetMissilText();
            if (ammo <= 0 && Time.time > last_Fire_Time + 30f)
            {
                ammo = 5;
                //SetBulletText();
                Debug.Log("Reloaded");
            }
            if (Input.GetKey(keyFire) )
            {
                if (ammo > 0)
                {
                    FireGun();
                }
            }
        }
    
        void FireGun()  // tiro da arma
        {
            if (Time.time > last_Fire_Time + frequency) // tempo entre tiros
            {
                GameObject bullet = Instantiate(missilPrefab, spawnPoint.position, spawnPoint.rotation); // instanciacao da bala
                ammo--;
                //SetMissilText();
                last_Fire_Time = Time.time; //forma que verificar a ultima vez que foi dispardo
                GetComponent<FMODUnity.StudioEventEmitter>().Play();
            }
            if (ammo == 0)
            {
                Debug.Log("Sem Ammo");
            }
        }
    
        void SetMissilText() // ui numero de balas
        {
            
            if (_missilText)  // se o texto existe
            {
                if (ammo == 0)
                {
                    _missilText.text = "Reloading...";
                }else
                    _missilText.text = "Missiles: " + ammo; // mostra na UI
            }
        }
    }
}