﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletPlayer : MonoBehaviour
{

    public float speed = 50f;
    public float lifeTime = 1f;
    public float dist = 200;

    public LayerMask enemyMask;
    private float damageRadius = 0.3f;

    private HP _enemyHp;
    
    void Start()
    {
        Destroy(gameObject, lifeTime);
        _enemyHp = GameObject.FindGameObjectWithTag("Vehicle2").GetComponent<HP>();
    }


    void Update()
    {
        MoveBullet();
    }


    void MoveBullet()
    {
        Vector3 temp = transform.position;
        temp += transform.forward * speed * Time.deltaTime;
        transform.position = temp;
        dist -= speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemyTarget"))
        {
            _enemyHp.RemoveHp(1);
            Destroy(gameObject);
            //Debug.Log("Bala OK");
        }
    }
}