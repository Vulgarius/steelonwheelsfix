﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Range (1, 50)] public int bullet_damage; //Dano causado pela bala
    public Rigidbody bulletRigidbody; // aceder ao ridigbody
    public Collider bullet_collider; //Colisão associada a bala
    [Range(0, 3)] public int bullet_type; //Tipo de bala
    private Transform missile_target = null; //Alvo decidido pelo míssil
    //public Rigidbody vehicle_rigidbody; //O rigidbody do veículo : utilizado para garantir que a velocidade da bala é sempre comparável à do veículo

    [Header("Parameters")]     // parametros com range
    [Range(5f, 500.0f)] public float speedBullet; // velocidade da bala q
    
    void Start()
    {
        bulletRigidbody = GetComponent<Rigidbody>();

        if (bulletRigidbody) // se a bala estiver disponivel
        {
            bulletRigidbody.velocity = transform.forward * speedBullet/* * ((vehicle_rigidbody.velocity.z + 1f) * (vehicle_rigidbody.velocity.y + 1f))*/; // aplica a forca ao corpo da bala na direcao frontal
        }
    }

    void Update()
    {
        switch (bullet_type)
        {
            case 3 : //Tiro teleguiado
                if (missile_target != null) //Se possuir alvo
                {
                   
                }
                break;
            case 2 :

                break;
            default : //Tiro normal
                break;
        }
        
    }

    public void Aiming(Transform _target)
    {
        missile_target = _target;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Vehicle") //Testa se a bala atingiu um objeto com a tag "Vehicle"
        {
            collision.gameObject.GetComponent<Vehicle>().Damage(2, bullet_damage, speedBullet); //Veículo sendo atingido
        }
        if ((bullet_type == 3) && (missile_target != null))
        {
            missile_target = null;
        }
        Destroy(this.gameObject); //Destrói a bala sob impacto
    }
}
