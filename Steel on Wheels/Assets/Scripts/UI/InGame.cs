﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class InGame : MonoBehaviour
    {
        public Text[] namesText;
        public string a, b, c, d;

        // Update is called once per frame
        void Update()
        {
            // em cada momento assigna o texto a posicao dos jogadores
            namesText[0].text = a;
            namesText[1].text = b;
            namesText[2].text = c;
            namesText[3].text = d;
        }
    }
}
