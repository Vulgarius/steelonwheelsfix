﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class paragemTempo : MonoBehaviour
{
   // counter newCounter = new counter();
    
    private bool isRunning = true;

    

    private float currentTime;
    private float startTime;
    
    [SerializeField] private Text counterTimer;
    void Start()
    {
        startTime = Time.time;
    }

    
    void Update()
    {

        if (isRunning)
        {
            float t = Time.time - startTime;
       
            string minutes = ((int) t / 60).ToString();
            string seconds = (t % 60).ToString("f2");

            counterTimer.text = minutes + ":" + seconds;   
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Vehicle"))
        {
            isRunning = false;
        }
    }
}
