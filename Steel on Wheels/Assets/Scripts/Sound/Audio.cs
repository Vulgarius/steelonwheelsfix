﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class Audio : MonoBehaviour
{
    public AudioSource[] music_source, sfx_source; //Daonde sai o áudio e efeitos sonoros
    public AudioClip[] music_clips, sfx_clips; //Clipes de música e efeitos sonoros
    private Thread sfx; //Thread responsável por tocar efeitos sonoros
    
    // Start is called before the first frame update
    void Start()
    {
        sfx = new Thread(new ParameterizedThreadStart(SFXPlayer)); //Inicia a thread
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SFX(int sfx_index, int source_index) //Muda o efeito sonoro
    {
        sfx_source[source_index].clip = sfx_clips[sfx_index]; //Altera o efeito sonoro
        sfx.Start(source_index); //Inicia a thread
    }

    void SFXPlayer(object source_index) //Responsável por tocar efeitos sonoros
    {
        sfx_source[(int)source_index].Play(); //Ativa o efeito sonoro
    } 
}
