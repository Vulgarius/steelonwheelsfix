﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Random = System.Random;

public class Carro_IA : MonoBehaviour
{

    public enum States
    {
        Drive,
        Arena,
        Attack
    }
    
    Carro_TesteIa carro;
    public int atual = 0;
    float virar = 0;
    
    private GameObject Arena;

    private Waypoint[] arenaWaypoints;
    private Waypoint[] raceWaypoints;
    private Waypoint[] TargetPoint;
    
    private bool followAtRandom;

    public States currenState;

    private SphereCollider sphere;

    private void Start()
    {
        GameObject parentWaypoints = GameObject.Find("WayP");
        GameObject parentWaypointsArena = GameObject.Find("WapointsArena");
        GameObject parentTarget = GameObject.Find("targetWay");

        raceWaypoints = parentWaypoints.GetComponentsInChildren<Waypoint>();
        if (parentWaypointsArena != null) 
            arenaWaypoints = parentWaypointsArena.GetComponentsInChildren<Waypoint>();
        if (parentTarget != null) 
            TargetPoint = parentTarget.GetComponentsInChildren<Waypoint>(); ;

        carro = GetComponent<Carro_TesteIa>();

        sphere = gameObject.GetComponent<SphereCollider>();
        

    }

    private void FixedUpdate()
    {
        //Define o que será feito em cada estado da IA.
        switch (currenState)
        {
            case States.Drive:
            {
                FollowWaypoits(raceWaypoints, false);
            }
                break;

            case States.Arena:
            {
                FollowWaypoits(arenaWaypoints, true);
                sphere.radius = 4.5f;
            }
                break;

            case States.Attack:
            {
                FollowWaypoits(TargetPoint, false);
            }
                break;
        }
    }

    ////////////////////////DURANTE A CORRIDA

        private void FollowWaypoits(Waypoint[] waypoints, bool followAtRandom) // comportamento ate a arena
        {
            //Forward and Backward
            if (waypoints[atual].velocidaderecomendada > carro.KmHora() - 10)
            {
                //Debug.Log(listaWaypoints[atual].velocidaderecomendada);
                carro.GO(1, 0); //acelerar
            }
            else if ((waypoints[atual].velocidaderecomendada < carro.KmHora() + 10))
            {
                carro.GO(-1, 1); //freiar
            }

            Vector3 dir = transform.InverseTransformPoint(new Vector3(waypoints[atual].transform.position.x,
                transform.position.y, waypoints[atual].transform.position.z));

            virar = Mathf.Clamp((dir.x / dir.magnitude) * 10f, -1f, 1f);

            //Steer
            if (virar > 0)
            {
                if (virar < 0.3)
                {
                    carro.Steering(0.3f);
                }
                else if (virar > 0.3 && virar < 0.7)
                {
                    carro.Steering(0.7f);
                }
                else
                {
                    carro.Steering(1);
                }
            }
            else
            {
                if (virar > -0.3)
                {
                    carro.Steering(-0.3f);
                }
                else if (virar < -0.3 && virar > -0.7)
                {
                    carro.Steering(-0.7f);
                }
                else
                {
                    carro.Steering(-1);
                }
            }


            if (Vector3.Distance(transform.position, waypoints[atual].transform.position) < 10f && currenState != States.Attack)
            {
                if (followAtRandom == false)
                {
                    if (atual <= waypoints.Length)
                    {
                        atual++;
                    }
                }

                if (followAtRandom)
                {
                    atual = UnityEngine.Random.Range(0, waypoints.Length - 1);
                }
            }
        }
}






        
    
    
    


        
    
    
    

