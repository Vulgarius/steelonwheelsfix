﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint_Creator : MonoBehaviour
{
    public GameObject Waypoint;
    Drive1 carro;

    bool criar = false;

    private void Start()
    {
        carro = GetComponent<Drive1>();
    }

    IEnumerator colocar()
    {
        yield return new WaitForSeconds(0.6f);
        GameObject way = Instantiate(Waypoint, transform.position, Quaternion.identity);
        way.GetComponent<Waypoint>().velocidaderecomendada = carro._velocidadeInstantanea;
        if (criar)
        {
            StartCoroutine(colocar());
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            criar = !criar;
            if (criar)
            {
                StartCoroutine(colocar());
            }
        }
    }
}
