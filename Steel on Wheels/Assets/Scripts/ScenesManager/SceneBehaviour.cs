﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    public bool load_scene_test; //Testa se uma cena deve ser carregada
    public int SceneToLoad_Level; //O nível q deve ser carregado
    void Start()
    {
        if (load_scene_test)
        {
            SceneManager.LoadSceneAsync(SceneToLoad_Level, LoadSceneMode.Additive);
        }
    }

    public void SceneChange(string now, string next, LoadSceneMode scene_mode) //Muda a cena atualmente carregada
    {
        SceneManager.UnloadSceneAsync(now);
        SceneManager.LoadSceneAsync(next, scene_mode);
    }
}
