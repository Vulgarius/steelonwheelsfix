﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;



public class Drive1 : MonoBehaviour
{
    
    [Header("Efeitos Sonoros")]
    public AudioClip somCarro;
    public AudioSource audioCarro;
    
    public WheelCollider[] wCs; // colliders
    public GameObject[] wheel; //forma de assinar a mesh do pneu
    
    [Header("Carro Variaveis")]
    public float torque = 220; // forca de rotacao aplicada a pequena esfera q se situa por baixo da roda

    public float maxSteerAngle = 45; //movimento lateral das rodas
    public float maxBrakeTorque = 500; // capacidade do travao

    private Rigidbody _rb;
    public Vector3 centerOffMass = new Vector3(0, 0, 0);

    [SerializeField] private Text speedLimit; // ui para mostrar a velocidade

    public float _variavelDeVelocidade = 10;

    public float _velocidadeInstantanea; // qual a velocidade a cada momento do rigidbody
    public GameObject velocimetro_ponteiro; //Ponteiro do velocímetro

    // luzes de velocidade
    // private GameObject _lightGreen;
    // private GameObject _lightYellow;
    // private GameObject _lightRed;

    //luz de travao
    // private GameObject[] _luzDeTravao;
    // private GameObject[] _luzDeTrasNormal;

    //luz da frente
    public GameObject[] luzDaFrente;


    /// <summary>
    /// //////////////////////////////////////////
    /// </summary>
    private WheelFrictionCurve forwardFriction, sidewaysFriction;

    [HideInInspector] public float KPH;
    public float handBrakeFrictionMultiplier = 2f;
    private float radius = 6;
    private float driftFactor;
    private float downForceValue = 10f;
    
    /// <summary>
    /// ///////////////////////////////////////////////////////
    /// </summary>



    void Start()
    {
        
        
        
        _rb = GetComponent<Rigidbody>();
        _rb.centerOfMass = centerOffMass; // centro de massa de forma a dar estabilidade ao carro

        // _lightGreen = GameObject.FindGameObjectWithTag("LuzVerde").gameObject;
        // _lightYellow = GameObject.FindGameObjectWithTag("LuzAmarela").gameObject;
        // _lightRed = GameObject.FindGameObjectWithTag("LuzVermelha").gameObject;

        // _luzDeTravao = GameObject.FindGameObjectsWithTag("LuzDeTravao");
        // _luzDeTrasNormal = GameObject.FindGameObjectsWithTag("LuzTraseiraNormal");

        luzDaFrente = GameObject.FindGameObjectsWithTag("LuzDaFrente");

        audioCarro.clip = somCarro;
    }

    void GO(float acceleration, float steer, float brake)
    {
        acceleration =
            Mathf.Clamp(acceleration, -1, 1); // funciona como percentagem entre o minimo e o maximo do fullTorque
        //steer = Mathf.Clamp(steer, -1, 1) * maxSteerAngle; // alteracao da direcao horizontal da roda de modo a virar a roda o maximo de 30 graus
        brake = Mathf.Clamp(brake, 0, 1) * maxBrakeTorque; // capacidade de travar o caro que vai se 0 a 500 

        float thrustTorque = acceleration * torque;





        for (int i = 0; i < 4; i++) // verifica as 4 rodas
        {
            wCs[i].motorTorque = thrustTorque; // forca aplicada as 4 rodas

            if (i < 2) // aplicar rotacao as primeiras duas rodas, que estao selecionadas apenas as primeiras duas.
            {
                //wCs[i].steerAngle = steer; // possibilita a roda de mudar de direcao  
                wCs[i].brakeTorque = brake; // aplica travao aos pneus de tras
                
                if (steer > 0)
                {
                    //rear tracks size is set to 1.5f       wheel base has been set to 2.55f
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * steer;
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * steer;
                }
                else if (steer < 0)
                {
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * steer;
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * steer;
                    //transform.Rotate(Vector3.up * steerHelping);

                }
                else
                {
                    wCs[i].steerAngle = 0;
                    wCs[i].steerAngle = 0;
                }
                
                
            }
            else
            {
                wCs[i].motorTorque =
                    (thrustTorque / 2.85f); // forca aplicada as 2 rodas de tras mas a forca aplicada 3x menor 
            }

            // movimento do mesh da roda
            Quaternion quat; //rotacao
            Vector3 position; //posicao

            wCs[i].GetWorldPose(out position, out quat); // aplica a posicao do objecto rotacap
            wheel[i].transform.rotation = quat;
            //Wheel[i].transform.position = position;
        }
    }

    float KmHora()
    {
        float aux = _rb.velocity.magnitude; // permite saber qual o valor da velocidade do objecto neste caso do rigidbody
        velocimetro_ponteiro.transform.rotation = Quaternion.Euler(0, 0, -_velocidadeInstantanea);//Transforma a rotação do ponteiro para refletir a velocidade do jogador
        return _velocidadeInstantanea = (aux * 1000 / 3600) * _variavelDeVelocidade;
    }

    void FixedUpdate()
    {
         float a = Input.GetAxisRaw("Player2Vertical"); // qd pressionas as teclas W e S
         float s = Input.GetAxisRaw("Player2Horizontal"); // qd pressionas as teclas A e D
         float b = Input.GetAxis("Jump2"); //quando preciono a tecla espacosss sss

        GO(a, s,b); // imputs do jogador 
        ActiveLights(b, s);

        speedLimit.text = KmHora().ToString("f0") + " Km/h";

        //SwitchColor(); // mudanca de cor 


        adjustTraction(s,b);
        //steerVehicle(s);
        //addDownForce();

        audioCarro.pitch = 0.6f + _velocidadeInstantanea / 60f;

    }

    // private void SwitchColor()
    // {
    //     if (_velocidadeInstantanea >= 0 && _velocidadeInstantanea < 55)
    //     {
    //         _lightGreen.SetActive(true);
    //         _lightYellow.SetActive(false);
    //         _lightRed.SetActive(false);
    //     }
    //     else if (_velocidadeInstantanea >= 55 && _velocidadeInstantanea < 90)
    //     {
    //         _lightGreen.SetActive(false);
    //         _lightYellow.SetActive(true);
    //         _lightRed.SetActive(false);
    //     }
    //     else if (_velocidadeInstantanea >= 90)
    //     {
    //         _lightGreen.SetActive(false);
    //         _lightYellow.SetActive(false);
    //         _lightRed.SetActive(true);
    //     }
    // }

    private void adjustTraction(float steer, float brake)
    {
        //tine it takes to go from normal drive to drift 
        float driftSmothFactor = .7f * Time.deltaTime;

        if (brake> 0)
        {
            sidewaysFriction = wCs[0].sidewaysFriction;
            forwardFriction = wCs[0].forwardFriction;

            float velocity = 0;
            sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue = forwardFriction.extremumValue =
                forwardFriction.asymptoteValue =
                    Mathf.SmoothDamp(forwardFriction.asymptoteValue, driftFactor * handBrakeFrictionMultiplier,
                        ref velocity, driftSmothFactor);

            for (int i = 0; i < 4; i++)
            {
                wCs[i].sidewaysFriction = sidewaysFriction;
                wCs[i].forwardFriction = forwardFriction;
            }

            sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue =
                forwardFriction.extremumValue = forwardFriction.asymptoteValue = 1.1f;
            //extra grip for the front wheels
            for (int i = 0; i < 2; i++)
            {
                wCs[i].sidewaysFriction = sidewaysFriction;
                wCs[i].forwardFriction = forwardFriction;
            }

            _rb.AddForce(transform.forward * (KPH / 400) * 10000);
        }
        //executed when handbrake is being held
        else
        {

            forwardFriction = wCs[0].forwardFriction;
            sidewaysFriction = wCs[0].sidewaysFriction;

            forwardFriction.extremumValue = forwardFriction.asymptoteValue = sidewaysFriction.extremumValue =
                sidewaysFriction.asymptoteValue =
                    ((KPH * handBrakeFrictionMultiplier) / 300) + 1;

            for (int i = 0; i < 4; i++)
            {
                wCs[i].forwardFriction = forwardFriction;
                wCs[i].sidewaysFriction = sidewaysFriction;

            }
        }
        
        //checks the amount of slip to control the drift
        for(int i = 2;i<4 ;i++){

            WheelHit wheelHit;
            
            wCs[i].GetGroundHit(out wheelHit);
            

            if(wheelHit.sidewaysSlip < 0 )	driftFactor = (1 + -steer) * Mathf.Abs(wheelHit.sidewaysSlip) ;

            if(wheelHit.sidewaysSlip > 0 )	driftFactor = (1 + steer )* Mathf.Abs(wheelHit.sidewaysSlip );
        }	
    }

/*
    private void addDownForce()
    {
        _rb.AddForce(-transform.up * downForceValue * _rb.velocity.magnitude);
    }
    */

    private void ActiveLights(float brake, float steer)
    {
        // for (int i = 0; i < 2; i++)
        // {
        //     if (brake > 0) // se o valor do travao for maior que 0
        //     {
        //         _luzDeTravao[i].SetActive(true);
        //         _luzDeTrasNormal[i].SetActive(false);
        //
        //     }
        //     else
        //     {
        //         _luzDeTrasNormal[i].SetActive(true);
        //         _luzDeTravao[i].SetActive(false);
        //     }
        //
        // }

        for (int j = 0; j < 2; j++)
        {
            luzDaFrente[j].SetActive(false); // por defeito as luzes extra estao apagadas
            if (j == 0)
            {
                if (steer > 30) // se a direcao para a direita for superior a 30 graus
                {
                    luzDaFrente[j].SetActive(true); // luz desse lado ativa
                }
            }
            else
            {
                if (steer < -30) // se a direcao para a direita for superior a -30 graus
                {
                    luzDaFrente[j].SetActive(true);
                }
            }
        }
    }
}
    

