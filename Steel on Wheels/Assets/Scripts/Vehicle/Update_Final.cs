﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Update_Final : MonoBehaviour
{
    Carro_TesteIa carro;
    private void Start()
    {
        carro = GetComponent<Carro_TesteIa>();
    }
    void FixedUpdate()
    {
        carro.a = Input.GetAxisRaw("Player1Vertical"); // qd pressionas as teclas W e S
        carro.s = Input.GetAxisRaw("Player1Horizontal"); // qd pressionas as teclas A e D
        carro.b = Input.GetAxis("Jump"); //quando preciono a tecla espacosss sss

        carro.GO(carro.a, carro.b); // imputs do jogador 
        //ActiveLights(b, s);

        carro.speedLimit.text = carro.KmHora().ToString("f0") + " Km/h";

        //SwitchColor(); // mudanca de cor 


        carro.adjustTraction(carro.s, carro.b);
        //steerVehicle(s);
        //addDownForce();
    }
}
