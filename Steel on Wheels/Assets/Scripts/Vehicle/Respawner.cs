﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour
{

    private GameObject _vehicle;
    private GameObject _camera;
    private Rank _rank;
    private Rigidbody _vehicle1;

    // Start is called before the first frame update
    void Start()
    {
        _vehicle = GameObject.FindGameObjectWithTag("Vehicle");
        _camera = GameObject.FindGameObjectWithTag("MainCamera");
        _rank = GameObject.FindGameObjectWithTag("CheckPointManager").GetComponent<Rank>();
        _vehicle1 = _vehicle.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && _rank.checkpoint != null)
        {
            Respawn();
        }
    }

    private void Respawn()
    {
        var transform1 = _rank.checkpoint.transform;
        var position = transform1.position;
        var rotation = transform1.rotation;
        _vehicle.transform.position = position;
        _vehicle.transform.rotation = rotation;
        _camera.transform.position = position;
        _camera.transform.rotation = rotation;
        _vehicle1.velocity = Vector3.zero;
        _vehicle1.angularVelocity = Vector3.zero;
        //_rank._p1Cp--;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("KillPlane"))
        {
            Respawn();
        }
    }
    
}
