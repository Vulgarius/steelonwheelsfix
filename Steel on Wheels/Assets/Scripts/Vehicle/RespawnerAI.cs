﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnerAI : MonoBehaviour
{

    private GameObject _vehicle;
    private Rank _rank;
    private Rigidbody _vehicle1;
    private Carro_TesteIa _carro;


    // Start is called before the first frame update
    void Start()
    {
        _vehicle = GameObject.FindGameObjectWithTag("Vehicle2");
        _rank = GameObject.FindGameObjectWithTag("CheckPointManager").GetComponent<Rank>();
        _vehicle1 = _vehicle.GetComponent<Rigidbody>();
        _carro = GetComponent<Carro_TesteIa>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_rank.checkpointAI != null && _carro.KmHora() < 0.5f)
        {
            Respawn();
        }
    }

    private void Respawn()
    {
        var transform1 = _rank.checkpointAI.transform;
        var position = transform1.position;
        var rotation = transform1.rotation;
        _vehicle.transform.position = position;
        _vehicle.transform.rotation = rotation;
        _vehicle1.velocity = Vector3.zero;
        _vehicle1.angularVelocity = Vector3.zero;
        //_rank._p2Cp--;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("KillPlane"))
        {
            Respawn();
        }
    }
    
}
