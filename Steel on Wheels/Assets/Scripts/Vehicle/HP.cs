﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour
{

    [SerializeField]
    private Text _playerHP;
    
    public int maxHp = 200, currentHp = 200;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHp <= 0)
        {
            Destroy(gameObject);
        }
        SetHPText();
    }

    public void RemoveHp(int damage)
    {
        currentHp -= damage;
        if (gameObject.CompareTag("Vehicle"))
        {
            Debug.Log("HP Player: " + currentHp);

        }else if (gameObject.CompareTag("Vehicle2"))
        {
            Debug.Log("HP IA: " + currentHp);
        }
    }
    
    void SetHPText() // ui numero de balas
    {
        if (_playerHP)  // se o texto existe
        {
            _playerHP.text = "Player HP: " + currentHp;
        }
    }
    
}
