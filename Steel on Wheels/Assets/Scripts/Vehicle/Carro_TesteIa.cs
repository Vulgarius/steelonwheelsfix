﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class Carro_TesteIa : MonoBehaviour
{
    public float a;
    public float s;
    public float b;
    public float thrustTorque;
    public WheelCollider[] wCs; // colliders
    public GameObject[] wheel; //forma de assinar a mesh do pneu

    [Header("Carro Variaveis")]
    public float torque = 220; // forca de rotacao aplicada a pequena esfera q se situa por baixo da roda

    public float maxSteerAngle = 45; //movimento lateral das rodas
    public float maxBrakeTorque = 500; // capacidade do travao

    private Rigidbody _rb;
    public Vector3 centerOffMass = new Vector3(0, 0, 0);

    public Text speedLimit; // ui para mostrar a velocidade

    public float _variavelDeVelocidade = 10;

    public float _velocidadeInstantanea; // qual a velocidade a cada momento do rigidbody

    // luzes de velocidade
    private GameObject _lightGreen;
    private GameObject _lightYellow;
    private GameObject _lightRed;

    //luz de travao
    private GameObject[] _luzDeTravao;
    private GameObject[] _luzDeTrasNormal;

    //luz da frente
    public GameObject[] luzDaFrente;


    /// <summary>
    /// //////////////////////////////////////////
    /// </summary>
    private WheelFrictionCurve forwardFriction, sidewaysFriction;

    [HideInInspector] public float KPH;
    public float handBrakeFrictionMultiplier = 2f;
    private float radius = 6;
    private float driftFactor;
    private float downForceValue = 10f;

    /// <summary>
    /// ///////////////////////////////////////////////////////
    /// </summary>

    // sensores /////////////////////////////////////////////////////
    [Header("Sensor")]
    public float sensorLength = 7.0f; //cumprimento dos sensores
    public Vector3 frontSensorPosition =  new Vector3(0.4f, 0.0f, 0.4f); //local de inicio do sensor frontal
    public float frontSideSensorPosition = 0.05f; //sensores frontais laterais
    public float frontSensorAngle = 30; //sensores laterais com amplitute

    public bool avoiding = false; //o que evitar
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.centerOfMass = centerOffMass; // centro de massa de forma a dar estabilidade ao carro

        // _lightGreen = GameObject.FindGameObjectWithTag("LuzVerde").gameObject;
        // _lightYellow = GameObject.FindGameObjectWithTag("LuzAmarela").gameObject;
        // _lightRed = GameObject.FindGameObjectWithTag("LuzVermelha").gameObject;
        //
        // _luzDeTravao = GameObject.FindGameObjectsWithTag("LuzDeTravao");
        // _luzDeTrasNormal = GameObject.FindGameObjectsWithTag("LuzTraseiraNormal");

        luzDaFrente = GameObject.FindGameObjectsWithTag("LuzDaFrente");
    }

    public void Steering(float steer)
    {
        for (int i = 0; i < 4; i++)
        {
            if (i < 2)
            {
                if (steer > 0)
                {
                    //rear tracks size is set to 1.5f       wheel base has been set to 2.55f
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * steer;
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * steer;
                }
                else if (steer < 0)
                {
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius - (1.5f / 2))) * steer;
                    wCs[i].steerAngle = Mathf.Rad2Deg * Mathf.Atan(2.55f / (radius + (1.5f / 2))) * steer;
                    //transform.Rotate(Vector3.up * steerHelping);
                }
                else
                {
                    wCs[i].steerAngle = 0;
                    wCs[i].steerAngle = 0;
                }
            }
            // movimento do mesh da roda
            Quaternion quat; //rotacao
            Vector3 position; //posicao

            wCs[i].GetWorldPose(out position, out quat); // aplica a posicao do objecto rotacap
            wheel[i].transform.rotation = quat;
            //Wheel[i].transform.position = position;
        }
        ///////////////////SENSORES///////////////////////////////////////////////////////////////////////////////////////
        RaycastHit hit; //criacao do raycaste que vai detatar colisoes
        Vector3 sensorStartPos = transform.position; // o valor de posicao do sensor vai ser igual a transfomada do carro
        sensorStartPos += transform.forward * frontSensorPosition.z; // faz com que os sensores se movam consoante a localizacao frontal do carro
        sensorStartPos += transform.up * frontSensorPosition.y; // faz com que o sensor se posicione um pouco a cima no eixo dos y
        //sensorStartPos.z += frontSensorPosition; // o sensor frontal vai da posicao do transform do carro no eixo z mais 0.5f
        float avoidMultiplier = 0f; // vai determinar se colide com algo no lado positivo ou no lado negativo
        avoiding = false; // sempre que inicia a funcao
            
     

        // sensor do lado direito
        sensorStartPos += transform.right * frontSideSensorPosition;
        if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) //o sensor vai da posicao inicial, na direccao frontal, ate encontrar algum obstaculo, com o cumprimento do sensor
        {
            if (!hit.collider.CompareTag("Road") && !hit.collider.CompareTag("CheckPoint") && !hit.collider.CompareTag("CheckPoint2")) // se colidir com a estrada
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                avoiding = true; // significa que collidir com algo que nao a estrada
                avoidMultiplier -= 2.0f;
            }
        }
        
        //vetor frontal lado direito usasse o quaternion.angleAxis com o anglo que pretendemos e o vector perpendicular * o vator que queremos apontar( desrta forma temos o vector 3 da direccao pretendida)
      else  if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) //o sensor vai da posicao inicial, na direccao frontal, ate encontrar algum obstaculo, com o cumprimento do sensor
        {
            if (!hit.collider.CompareTag("Road") && !hit.collider.CompareTag("CheckPoint") && !hit.collider.CompareTag("CheckPoint2")) // se colidir com a estrada
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                avoiding = true; // significa que collidir com algo que nao a estrada
                avoidMultiplier -= 1.0f;
            }
        }
        
        //vetor frontal lado esquerdo
        sensorStartPos -= transform.right * frontSideSensorPosition * 2;
        if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) //o sensor vai da posicao inicial, na direccao frontal, ate encontrar algum obstaculo, com o cumprimento do sensor
        {
            if (!hit.collider.CompareTag("Road") && !hit.collider.CompareTag("CheckPoint") && !hit.collider.CompareTag("CheckPoint2")) // se colidir com a estrada
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                avoiding = true; // significa que collidir com algo que nao a estrada
                avoidMultiplier += 2.0f;
            }
        }
        
        //vetor frontal lado esquerdo com amplitude de 30 // neste caso apenas e necessario colocar o angulo de amplitude 30 negativo
       else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) //o sensor vai da posicao inicial, na direccao frontal, ate encontrar algum obstaculo, com o cumprimento do sensor
        {
            if (!hit.collider.CompareTag("Road") && !hit.collider.CompareTag("CheckPoint") && !hit.collider.CompareTag("CheckPoint2")) // se colidir com a estrada
            {
                Debug.DrawLine(sensorStartPos, hit.point);
                avoiding = true; // significa que collidir com algo que nao a estrada
                avoidMultiplier += 1.0f;
            }
        }
        
        //vetor frontal
        //o sensores vao da posicao inicial, na direccao frontal, ate encontrar algum obstaculo, com o cumprimento do sensor
        if (avoidMultiplier == 0)
        {
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
            {
                if (!hit.collider.CompareTag("Road") && !hit.collider.CompareTag("CheckPoint") && !hit.collider.CompareTag("CheckPoint2")) // se colidir com a estrada
                {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoiding = true; // significa que collidir com algo que nao a estrada

                    if (hit.normal.x < 0)
                    {
                        avoidMultiplier = -2;
                    }
                    else
                    {
                        avoidMultiplier = 2;
                    }
                }
            }
        }
        if (avoiding) // se colidiu com alguma coisa
        {
            for(int i=0; i<4; i++)
            {
                if (i < 2)
                {
                    if (steer > 0)
                    {
                        wCs[i].steerAngle= maxSteerAngle * avoidMultiplier;
                    }

                    if (steer < 0)
                    {
                        wCs[i].steerAngle = maxSteerAngle * avoidMultiplier;
                    }
                  
                }
            }
        }
        /////////////////////////////////////////////////////SENSORES/////////////////////////////////////
    }
    
    public void GO(float acceleration, float brake)
    {
        acceleration =
            Mathf.Clamp(acceleration, -1, 1); // funciona como percentagem entre o minimo e o maximo do fullTorque
        brake = Mathf.Clamp(brake, 0, 1) * maxBrakeTorque; // capacidade de travar o caro que vai se 0 a 500 
        //steer = Mathf.Clamp(steer, -1, 1) * maxSteerAngle; // alteracao da direcao horizontal da roda de modo a virar a roda o maximo de 30 graus
        
        thrustTorque = acceleration * torque;

        for (int i = 0; i < 4; i++) // verifica as 4 rodas
        {
            wCs[i].motorTorque = thrustTorque; // forca aplicada as 4 rodas

            if (i < 2) // aplicar rotacao as primeiras duas rodas, que estao selecionadas apenas as primeiras duas.
            {
                //wCs[i].steerAngle = steer; // possibilita a roda de mudar de direcao  
                wCs[i].brakeTorque = brake; // aplica travao aos pneus de tras
                
            }
            else
            {
                wCs[i].motorTorque =
                    (thrustTorque / 2.85f); // forca aplicada as 2 rodas de tras mas a forca aplicada 3x menor 
            }
        }
    }

   public float KmHora()
    {
        float
            aux = _rb.velocity.magnitude; // permite saber qual o valor da velocidade do objecto neste caso do rigidbody
        return _velocidadeInstantanea = (aux * 1000 / 3600) * _variavelDeVelocidade;
    }

    void FixedUpdate()
    {
        //float a = Input.GetAxisRaw("Player1Vertical"); // qd pressionas as teclas W e S
        //float s = Input.GetAxisRaw("Player1Horizontal"); // qd pressionas as teclas A e D
        //float b = Input.GetAxis("Jump"); //quando preciono a tecla espacosss sss

        //GO(a, s, b); // imputs do jogador 
        //ActiveLights(b, s);

        //speedLimit.text = KmHora().ToString("f0") + " Km/h";

        //SwitchColor(); // mudanca de cor 


        //adjustTraction(s, b);
        //steerVehicle(s);
        //addDownForce();


    }

    //private void SwitchColor()
    //{
    //    if (_velocidadeInstantanea >= 0 && _velocidadeInstantanea < 55)
    //    {
    //        _lightGreen.SetActive(true);
    //        _lightYellow.SetActive(false);
    //        _lightRed.SetActive(false);

    //    }
    //    else if (_velocidadeInstantanea >= 55 && _velocidadeInstantanea < 90)
    //    {
    //        _lightGreen.SetActive(false);
    //        _lightYellow.SetActive(true);
    //        _lightRed.SetActive(false);

    //    }
    //    else if (_velocidadeInstantanea >= 90)
    //    {
    //        _lightGreen.SetActive(false);
    //        _lightYellow.SetActive(false);
    //        _lightRed.SetActive(true);

    //    }

    //}

    public void adjustTraction(float steer, float brake)
    {
        //tine it takes to go from normal drive to drift 
        float driftSmothFactor = .7f * Time.deltaTime;

        if (brake > 0)
        {
            sidewaysFriction = wCs[0].sidewaysFriction;
            forwardFriction = wCs[0].forwardFriction;

            float velocity = 0;
            sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue = forwardFriction.extremumValue =
                forwardFriction.asymptoteValue =
                    Mathf.SmoothDamp(forwardFriction.asymptoteValue, driftFactor * handBrakeFrictionMultiplier,
                        ref velocity, driftSmothFactor);

            for (int i = 0; i < 4; i++)
            {
                wCs[i].sidewaysFriction = sidewaysFriction;
                wCs[i].forwardFriction = forwardFriction;
            }

            sidewaysFriction.extremumValue = sidewaysFriction.asymptoteValue =
                forwardFriction.extremumValue = forwardFriction.asymptoteValue = 1.1f;
            //extra grip for the front wheels
            for (int i = 0; i < 2; i++)
            {
                wCs[i].sidewaysFriction = sidewaysFriction;
                wCs[i].forwardFriction = forwardFriction;
            }

            _rb.AddForce(transform.forward * (KPH / 400) * 10000);
        }
        //executed when handbrake is being held
        else
        {

            forwardFriction = wCs[0].forwardFriction;
            sidewaysFriction = wCs[0].sidewaysFriction;

            forwardFriction.extremumValue = forwardFriction.asymptoteValue = sidewaysFriction.extremumValue =
                sidewaysFriction.asymptoteValue =
                    ((KPH * handBrakeFrictionMultiplier) / 300) + 1;

            for (int i = 0; i < 4; i++)
            {
                wCs[i].forwardFriction = forwardFriction;
                wCs[i].sidewaysFriction = sidewaysFriction;

            }
        }

        //checks the amount of slip to control the drift
        for (int i = 2; i < 4; i++)
        {

            WheelHit wheelHit;

            wCs[i].GetGroundHit(out wheelHit);


            if (wheelHit.sidewaysSlip < 0) driftFactor = (1 + -steer) * Mathf.Abs(wheelHit.sidewaysSlip);

            if (wheelHit.sidewaysSlip > 0) driftFactor = (1 + steer) * Mathf.Abs(wheelHit.sidewaysSlip);
        }
    }

    /*
        private void addDownForce()
        {
            _rb.AddForce(-transform.up * downForceValue * _rb.velocity.magnitude);
        }
        */

    private void ActiveLights(float brake, float steer)
    {
        for (int i = 0; i < 2; i++)
        {
      if (brake > 0) // se o valor do travao for maior que 0
         {
               _luzDeTravao[i].SetActive(true);
              _luzDeTrasNormal[i].SetActive(false);

           }
           else
           {
               _luzDeTrasNormal[i].SetActive(true);
               _luzDeTravao[i].SetActive(false);
           }
      }

        for (int j = 0; j < 2; j++)
        {
            luzDaFrente[j].SetActive(false); // por defeito as luzes extra estao apagadas
            if (j == 0)
            {
                if (steer > 30) // se a direcao para a direita for superior a 30 graus
                {
                    luzDaFrente[j].SetActive(true); // luz desse lado ativa
                }
            }
            else
            {
                if (steer < -30) // se a direcao para a direita for superior a -30 graus
                {
                    luzDaFrente[j].SetActive(true);
                }
            }
        }
    }
    
   
}
