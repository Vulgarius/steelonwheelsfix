﻿using System.Collections;
using System.Collections.Generic;
using System;

using Level;
using UnityEngine;

public class Vehicle : MonoBehaviour
{

	// Main vehicle component

	public bool controllable; // Verifica se o carro é controlável ou não

	[Header("Components")] //componentes do veiculo

	public Transform vehicleModel; //tranformada do veiculo
	public Rigidbody sphere; //rig da esfera
	public Material car_material; //Material associado ao carro
	public Color car_color; //Cor do carro

	[Header("Controls")] // controlos de teclas de modo a ser modificado no inspector

	public KeyCode accelerate;
	public KeyCode brake;
	public KeyCode steerLeft;
	public KeyCode steerRight;
	//public KeyCode jump;
	public KeyCode smoker;
	public KeyCode backWards; // moviemnto para tras
	public KeyCode nitro;

	[Header("Parameters")] // parametros com range

	[Range(0, 1000)] public int health;
	[Range(5.0f, 40.0f)] public float acceleration = 30f;
	[Range(20.0f, 160.0f)] public float steering = 80f;
	//[Range(50.0f, 80.0f)] public float jumpForce = 60f;
	[Range(0.0f, 20.0f)] public float gravity = 10f;
	[Range(0.0f, 1.0f)] public float drift = 1f;
    [Range(0.0f, 600.0f)] public float nitroSpeed = 110f;
	[Range(1, 100)] public int car_resilience = 1;

    private float _stockNitro = 2.5f; //quntidade de nitro

    [Header("Switches")]  //verificadores

	public bool jumpAbility = false;
	public bool steerInAir = false;
	public bool motorcycleTilt = false;
	public bool alwaysSmoke = false;

	// Vehicle components

	Transform container, wheelFrontLeft, wheelFrontRight; // transformada pelo container e rodas da frente
	Transform body; // transformada do corpo
	TrailRenderer trailLeft, trailRight; // efeito das marcas das rodas

	ParticleSystem smoke; //efeito de particulas

	// Private

	float speed, speedTarget; //velocidade 
	float rotate, rotateTarget; //rotacao

	bool nearGround, onGround; //verifica se esta perto do chao ou se esta no chao

	Vector3 containerBase;

	private Checkpoint _checkPointManager;
	


	// Functions

	void Start()
	{
		car_material.color = car_color;
	}

	void Awake()
	{

		foreach (Transform t in GetComponentsInChildren<Transform>())
		{

			switch (t.name)
			{

				// Vehicle components

				case "wheelFrontLeft": wheelFrontLeft = t; break;
				case "wheelFrontRight": wheelFrontRight = t; break;
				case "body": body = t; break;

				// Vehicle effects

				case "smoke": smoke = t.GetComponent<ParticleSystem>(); break;
				case "trailLeft": trailLeft = t.GetComponent<TrailRenderer>(); break;
				case "trailRight": trailRight = t.GetComponent<TrailRenderer>(); break;

			}

		}

		container = vehicleModel.GetChild(0); // container e onde o body e as quatro rodas existem na hierarquia
		containerBase = container.localPosition;

	}

	void Update()
	{

		// Acceleration

		speedTarget = Mathf.SmoothStep(speedTarget, speed, Time.deltaTime * 12f);
		speed = 0f;
		if (controllable)
		{
			if (Input.GetKey(accelerate)) // se carrega na tecla selecionada
			{
				ControlAccelerate();
			}

			if (Input.GetKey(brake)) // se optar por travar
			{
				ControlBrake();
			}

			if (Input.GetKey(backWards)) // mover para tras
			{
				ControlBackWards();
			}

			if (Input.GetKeyDown(nitro)) //Nitro
			{

				ApplyNitro();
			}

		
		}
        // Steering

        rotateTarget = Mathf.Lerp(rotateTarget, rotate, Time.deltaTime * 4f); //mudar o 4f para deixar o cara mais leve ou pesado (mais ou menos "driftable")
		rotate = 0f;
		if (controllable)
		{
			if (Input.GetKey(steerLeft)) // se carregar na tecla S
			{
				ControlSteer(-1);   // direccao negativa  no eixo horizontal
			}

			if (Input.GetKey(steerRight)) // se carregar na tecla D
			{
				ControlSteer(1); // direccao positiva no eixo horizontal
			}
		}
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, transform.eulerAngles.y + rotateTarget, 0)), Time.deltaTime * 2.0f);

		// Jump

		//if(Input.GetKeyDown(jump))
		//{ ControlJump(); 
		//} 

		// Wheel and body tilt
		// quanto maior o valor de rotaTarget for dividido menos efeito de rotacao tem
		if (wheelFrontLeft != null) 
		{ 
			wheelFrontLeft.localRotation = Quaternion.Euler(0, rotateTarget, 0); // efeito de rotacao das rodas
		} 
		if (wheelFrontRight != null) 
		{ 
			wheelFrontRight.localRotation = Quaternion.Euler(0, rotateTarget, 0); 
		}

		body.localRotation = Quaternion.Slerp(body.localRotation, Quaternion.Euler(new Vector3(speedTarget / 8, 0, rotateTarget / 16)), Time.deltaTime * 4.0f);
		//speedTarget / 4 e rotateTarget / 6 mudar os numero para fazer o carro inclinar mais ou menos

		// Vehicle tilt

		float tilt = 0.0f;

		//if (motorcycleTilt)
		//{
		//	tilt = -rotateTarget / 1.5f;
		//}

		container.localPosition = containerBase + new Vector3(0, Mathf.Abs(tilt) / 2000, 0);
		container.localRotation = Quaternion.Slerp(container.localRotation, Quaternion.Euler(0, rotateTarget / 8, tilt), Time.deltaTime * 10.0f);

		// Effects

		//if (!motorcycleTilt)
		//{
		//	smoke.transform.localPosition = new Vector3(-rotateTarget / 100, smoke.transform.localPosition.y, smoke.transform.localPosition.z);
		//}

		//////////////////efeito de trail das rodas

		ParticleSystem.EmissionModule smokeEmission = smoke.emission;
		smokeEmission.enabled = onGround && sphere.velocity.magnitude > (acceleration / 4) && (Vector3.Angle(sphere.velocity, vehicleModel.forward) > 30.0f || alwaysSmoke);

		if (trailLeft != null)
		{
			trailLeft.emitting = smoke.emission.enabled;
		}

		if (trailRight != null)
		{
			trailRight.emitting = smoke.emission.enabled;
		}

		//  Always Smoke

		if (Input.GetKeyUp(smoker)) // se nao levantar a tecla
		{
			ControlSmoke();
		}

		// Stops vehicle from floating around when standing still

		if (speed == 0 && sphere.velocity.magnitude < 4f)
		{
			sphere.velocity = Vector3.Lerp(sphere.velocity, Vector3.zero, Time.deltaTime * 2.0f);
		}

	}

	// Physics update

	void FixedUpdate()
	{

		RaycastHit hitOn; //aplica um raycast com diferentes tamanhos para perceber se esta em contacto com o chao ou nao
		RaycastHit hitNear;

		var position = transform.position;
		onGround = Physics.Raycast(position, Vector3.down, out hitOn, 1.1f); // verifica se esta a tocar no chao
		nearGround = Physics.Raycast(position, Vector3.down, out hitNear, 2.0f); // verifica se esta perto do chao

		// Normal

		vehicleModel.up = Vector3.Lerp(vehicleModel.up, hitNear.normal, Time.deltaTime * 8.0f);
		vehicleModel.Rotate(0, transform.eulerAngles.y, 0);

		// Movement

		if (nearGround)
		{ // se estiver no chao

			sphere.AddForce(vehicleModel.forward * speedTarget, ForceMode.Acceleration); // aplica forca a esfera no eixo horizontal multiplicando pela acelaracao 

		}
		else
		{ // se a distancia do chao for maior do que 2.2f

			sphere.AddForce(vehicleModel.forward * (speedTarget / 10), ForceMode.Acceleration); // a velocidade e reduzida de modo a simular a gravidade

			// Simulated gravity

			sphere.AddForce(Vector3.down * gravity, ForceMode.Acceleration); // aplica a gravidade 

		}

		transform.position = sphere.transform.position + new Vector3(0, 0.35f, 0); // em cada momento a esfera e colocada no novo vector

		// Simulated drag on ground 

		Vector3 localVelocity = transform.InverseTransformVector(sphere.velocity);
		localVelocity.x *= 0.9f + (drift / 10);

		if (nearGround)
		{

			sphere.velocity = transform.TransformVector(localVelocity);

		}

		

	}

	// Controls

	public void ControlAccelerate()
	{ // aplica acelaracao
		
		speed = acceleration; // velocidade e igual a

	}

	public void ControlBrake()
	{ // se travar

		speed = 0; // a velocidade e zero

	}

	public void ControlBackWards() // se andar para tras
	{

		speed = -acceleration; // acelaracao e negativa
	}

    public void ApplyNitro() // nitro
    { // aplica acelaracao

        if (_stockNitro >= 0)
        {
            
            speed = nitroSpeed;
            _stockNitro -= 1f * Time.deltaTime;

        }

    }
    
    public void ControlJump()
    {

	    //	if(!controllable){ return; }

	    //	if(jumpAbility && nearGround){

	    //		sphere.AddForce(Vector3.up * (jumpForce * 20), ForceMode.Impulse);

	    //	}

	    //}
    }

    private void ControlSmoke()
	{
		if (alwaysSmoke)
			alwaysSmoke = false;
		else
			alwaysSmoke = true;
	}

	public void ControlSteer(int direction)
	{

		speed /= 3; //reducao da velocidade quando drift

		if ((speedTarget > 1 && speedTarget <= 30) || (speedTarget < -1 && speedTarget >= -30))
		{
			if (nearGround || steerInAir)
			{
				if (speedTarget < 0)
				{
					direction = -direction;
				}
				rotate = steering * direction;
			}
		}
	}

	// Hit objects

	void OnTriggerEnter(Collider other) // se colidir com outros objectos
	{

		if (other.GetComponent<PhysicsObject>()) 
		{ 
			other.GetComponent<PhysicsObject>().Hit(sphere.velocity);
		} // a velocidade mantem-se

		Damage(0, 0, 0); //Dano de colisão normal
	}

	// Functions

	public void SetPosition(Vector3 position, Quaternion rotation)
	{

		// Stop vehicle

		speed = rotate = 0.0f; // se a velocidade e a rotacao for 0

		sphere.velocity = Vector3.zero; //aplica o vector3.zero( 0, 0,0)
		sphere.position = position; // nova posicao vai ser onde a esfera se encontra

		// Set new position

		transform.position = position;
		transform.rotation = rotation;

	}

	public void Damage(int damage_type, int opponent_collider_damage, float opponent_acceleration)
	{
		int damage_taken = 0;
		switch (damage_type)
		{
			case 0 : //Dano causado por colisão
				damage_taken = Mathf.RoundToInt((1 * acceleration) / car_resilience); 
				break;
			case 1 : //Dano causado por inimigo colidindo contra o jogador
				damage_taken = Mathf.RoundToInt(((1 * opponent_acceleration) - opponent_collider_damage) / car_resilience);
				break;
			case 2 : //Dano causado por bala
				damage_taken = Mathf.RoundToInt((0.5f * opponent_collider_damage) / car_resilience);
				break;
		}
		if (damage_taken < 0)
		{
			damage_taken = 0;
		}
		health -= damage_taken; //Applies damage
	
		if (health <= 0) //If health pool has ended, car dies
		{
			Destroy(this.gameObject);
		}
	}

	
}