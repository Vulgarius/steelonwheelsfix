﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject[] menus;
    //public GameMode game_mode;
    public SceneBehaviour scene_behaviour;

    public void Start()
    {
        menus[0].SetActive(true);
        for (int i = 1; i < menus.Length; i++)
        {
            menus[i].SetActive(false);
        }
    }

    public void MenuChange(int next_menu)
    {
        for (int i = 0; i < menus.Length; i++)
        {
            if (menus[i].activeSelf)
            {
                menus[i].SetActive(false);
                break;
            }
        }
        menus[next_menu].SetActive(true);
    }

    public void GameSet(/*bool gamemode*/)
    {
        //game_mode.GameModeSet(gamemode);
        //scene_behaviour.SceneChange("Menus", "Fabio 2.0", UnityEngine.SceneManagement.LoadSceneMode.Single); //Muda a cena do menu para a escolhida
        SceneManager.LoadSceneAsync("Fabio 2.0", LoadSceneMode.Single);
        SceneManager.UnloadSceneAsync("Menus");
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}
