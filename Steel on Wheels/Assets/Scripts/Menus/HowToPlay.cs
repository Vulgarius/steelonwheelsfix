﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HowToPlay : MonoBehaviour
{
    public void ChamaCenaSairHowToPlay()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Opções");
    }
    
    public void ChamaCenaModoJogador()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("ModoJogador");
    }
    
}
