﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModoJogo : MonoBehaviour
{
    public void ChamaCenaSinglePlayer()
    {
        
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("New IA", LoadSceneMode.Single); //Carrega o modo de jogo escolhido
    }
    public void ChamaCenaMultiplayer()
    {
        
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Fábio New", LoadSceneMode.Single); //Carrega o modo de jogo escolhido
    }
    public void ChamaMenuPricipal()
    {
        
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("MenuInicial", LoadSceneMode.Single); //Carrega o menu principal
    }
}
