﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOpçoes : MonoBehaviour
{
    //Codigo que faz mudar de cenas
    public void ChamaCenaDoMenuPrincipal()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MenuInicial");
    }
    public void ChamaCenaDoHowToPlay()
    {
        StartCoroutine(SceneChangeWaiterHTP());
    }

    IEnumerator SceneChangeWaiterHTP()
    {
        //yield on a new YieldInstruction that waits for 0.5 seconds.
        yield return new WaitForSeconds(0.5f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("HowToPlay");
    }
    
}
