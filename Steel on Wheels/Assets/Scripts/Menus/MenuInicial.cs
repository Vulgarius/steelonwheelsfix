﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{
    public GameObject Musica_Cena;

    //Codigo que faz mudar de cenas
    public void ChamaCenaDoPlay()
    {
        StartCoroutine(SceneChangeWaiterStart());
    }
    public void ChamaCenaDasOpcoes()
    {
        StartCoroutine(SceneChangeWaiterOptions());
    }
    public void ChamaCenaDosCreditos()
    {
        StartCoroutine(SceneChangeWaiterCredits());
    }
    public void ChamaSairDoJogo()
    {
        StartCoroutine(SceneChangeWaiterExit());
    }
    
    IEnumerator SceneChangeWaiterStart()
    {
        //yield on a new YieldInstruction that waits for 0.5 seconds.
        yield return new WaitForSeconds(0.5f);
        //SceneManager.UnloadSceneAsync("MenuInicial");
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("HowToPlayReminder", LoadSceneMode/*.Additive*/.Single); //Carrega a próxima cena
    }
    
    IEnumerator SceneChangeWaiterOptions()
    {
        //yield on a new YieldInstruction that waits for 0.5 seconds.
        yield return new WaitForSeconds(0.5f);
        //SceneManager.UnloadSceneAsync("MenuInicial");
        UnityEngine.SceneManagement.SceneManager.LoadScene("Opções", LoadSceneMode/*.Additive*/.Single); //Carrega a próxima cena
    }
    
    IEnumerator SceneChangeWaiterCredits()
    {
        //yield on a new YieldInstruction that waits for 0.5 seconds.
        yield return new WaitForSeconds(0.5f);
        //SceneManager.UnloadSceneAsync("MenuInicial");
        UnityEngine.SceneManagement.SceneManager.LoadScene("CreditosGameplay", LoadSceneMode/*.Additive*/.Single); //Carrega a próxima cena
    }
    
    IEnumerator SceneChangeWaiterExit()
    {
        //yield on a new YieldInstruction that waits for 0.5 seconds.
        yield return new WaitForSeconds(0.5f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menus", LoadSceneMode/*.Additive*/.Single); //Carrega a próxima cena
    }
    
}
