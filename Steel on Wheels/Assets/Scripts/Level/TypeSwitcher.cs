﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeSwitcher : MonoBehaviour
{
    private GameObject _camaro, _nissan;
    private bool _measureInZ = true;
    private Text _playerRank;
    private GameObject[] _checkpointsp1;
    private GameObject[] _checkpointsp2;
    private int _p1Cp, _p2Cp;

    // Start is called before the first frame update
    void Start()
    {
        
        _camaro = GameObject.FindGameObjectWithTag("Vehicle");
        _nissan = GameObject.FindGameObjectWithTag("Vehicle2");
        _playerRank = GameObject.FindGameObjectWithTag("PlayerRank").GetComponent<Text>();
        _checkpointsp1 = GameObject.FindGameObjectsWithTag("CheckPoint");
        _checkpointsp2 = GameObject.FindGameObjectsWithTag("CheckPoint2");
        Debug.Log(_checkpointsp2.Length);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Vehicle") || other.gameObject.CompareTag("Vehicle2"))
        {
            _measureInZ = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_measureInZ)
        {
            if (_camaro.transform.position.z < _nissan.transform.position.z)
            {
                _playerRank.text = "1";
            }
            else
            {
                _playerRank.text = "2";
            }
        }
        
    }
}
