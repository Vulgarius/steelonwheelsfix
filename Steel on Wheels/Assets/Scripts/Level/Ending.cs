﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ending : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Vehicle"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuFinalAmerican", LoadSceneMode.Single); //Carrega a próxima cena

        }else if (other.gameObject.CompareTag("Vehicle2"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuFinalAsian", LoadSceneMode.Single); //Carrega a próxima cena
        }
    }
}
