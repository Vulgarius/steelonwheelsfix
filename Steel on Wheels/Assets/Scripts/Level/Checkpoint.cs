﻿using System;
using UnityEngine;

namespace Level
{
    public class Checkpoint : MonoBehaviour
    {
        private GameObject[] _checkPoints;

        private int _currentCheckPoint=1;

        private void Awake()
        {
            _checkPoints = GameObject.FindGameObjectsWithTag("CheckPoint"); // encontra os checkPoints
            _currentCheckPoint = 1;                             // assigna o inicio dos checkPoints
        }

        private void Start()
        {
            foreach (GameObject cp in _checkPoints)             // vai verificar cada checkPoints em cada interacao
            {
                cp.AddComponent<CurrentCheckPoint>();         // adiciona o componente checkPoint
                cp.GetComponent<CurrentCheckPoint>().currentCheckPNumber = _currentCheckPoint; // assigna o numero do checkPoint atual
                cp.name = "CheckPoint" + _currentCheckPoint; // rename checkPoint
                _currentCheckPoint++;                         // aumenta o numero de checkPoints para a proxima interacao 
            }
        }
    }
}

