﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class Rank : MonoBehaviour
{
    private GameObject _camaro, _nissan, _checkpointManager;
    private GameObject[] _checkpointsp1, _checkpointsp2, _checkpointsp3;
    public int _p1Cp, _p2Cp;
    public Transform checkpoint, checkpointAI;
    private Image[] _playerRank;
    public Sprite first, second;
    private Rank _rank;


    // Start is called before the first frame update
    void Start()
    {
        _camaro = GameObject.FindGameObjectWithTag("Vehicle");
        _nissan = GameObject.FindGameObjectWithTag("Vehicle2");
        _playerRank = GameObject.FindGameObjectWithTag("PlayerRank").GetComponentsInChildren<Image>();
        _checkpointsp1 = GameObject.FindGameObjectsWithTag("CheckPoint");
        _checkpointsp2 = GameObject.FindGameObjectsWithTag("CheckPoint2");
        _checkpointManager = GameObject.FindGameObjectWithTag("CheckPointManager");
        _rank = _checkpointManager.GetComponent<Rank>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (gameObject.CompareTag("Vehicle"))
        {
            if (other.gameObject.CompareTag("CheckPoint"))
            {
                _rank._p1Cp++;
                _rank.checkpoint = other.transform;
                other.GetComponent<BoxCollider>().enabled = false;
            }
        } else if (gameObject.CompareTag("Vehicle2"))
        {
            if (other.gameObject.CompareTag("CheckPoint2"))
            {
                _rank._p2Cp++;
                _rank.checkpointAI = other.transform;
                other.GetComponent<BoxCollider>().enabled = false;
            }
        }
        //Debug.Log("Player 1: " + _rank._p1Cp); 
        //Debug.Log("Player 2: " + _rank._p2Cp);
    }

    private void Update()
    {
        if (_rank._p1Cp > _rank._p2Cp)
        {
            _playerRank[1].enabled = false;
            _playerRank[0].enabled = true;
        }
        else
        {
            _playerRank[0].enabled = false;
            _playerRank[1].enabled = true;
        }
    }
}
