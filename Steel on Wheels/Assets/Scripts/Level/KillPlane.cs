﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlane : MonoBehaviour
{
    private Collider OldCamaro, OldNissan;
    private GameObject CameraCamaro, CameraNissan;
    private GameObject Respawn1;
    

    // Start is called before the first frame update
    void Start()
    {
        OldCamaro = GameObject.FindGameObjectWithTag("Vehicle").GetComponent<Collider>();
        OldNissan = GameObject.FindGameObjectWithTag("Vehicle2").GetComponent<Collider>();
        CameraCamaro = GameObject.Find("Camera Camaro").gameObject;
        CameraNissan = GameObject.Find("Camera Nissan").gameObject;
        
        Respawn1 = GameObject.FindGameObjectWithTag("Respawn");
    }

    // Update is called once per frame

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Vehicle"))
        {
            OldCamaro.transform.position = Respawn1.transform.position;
            OldCamaro.transform.rotation = Respawn1.transform.rotation;
            CameraCamaro.transform.position = Respawn1.transform.position;
            CameraCamaro.transform.rotation = Respawn1.transform.rotation;
            Rigidbody OldCamaroRB = OldCamaro.GetComponent<Rigidbody>();
            OldCamaroRB.velocity = Vector3.zero;
            OldCamaroRB.angularVelocity = Vector3.zero;
        }
        if (other.gameObject.CompareTag("Vehicle2"))
        {
            OldNissan.transform.position = Respawn1.transform.position;
            OldNissan.transform.rotation = Respawn1.transform.rotation;
            CameraNissan.transform.position = Respawn1.transform.position;
            CameraNissan.transform.rotation = Respawn1.transform.rotation;
            Rigidbody OldNissanRB = OldNissan.GetComponent<Rigidbody>();
            OldNissanRB.velocity = Vector3.zero;
            OldNissanRB.angularVelocity = Vector3.zero;
        }
    }
}
