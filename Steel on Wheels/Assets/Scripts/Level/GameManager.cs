﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UI;
using UnityEngine.SceneManagement;

namespace Level
{
    public class GameManager : MonoBehaviour
    {

        private GameObject _vehicle1, _vehicle2;
        
        void Start()
        {
            _vehicle1 = GameObject.FindGameObjectWithTag("Vehicle");
            _vehicle2 = GameObject.FindGameObjectWithTag("Vehicle2");
        }
        
        void Update()
        {
            if (!_vehicle1)
            {
                SceneManager.LoadScene("MenuFinalAsian", LoadSceneMode.Single); 
            }
            if (!_vehicle2)
            {
                SceneManager.LoadScene("MenuFinalAmerican", LoadSceneMode.Single);
            }
        }
    }
}



