﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node_Mixed : AI_Node
{
    public Node_Mixed(int agression, int friendliness, int greediness) :
        base(agression, friendliness, greediness)
    {

    }

    public override int CarMixed()
    {
        return 2;
    }
}
