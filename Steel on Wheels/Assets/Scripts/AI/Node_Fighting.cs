﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node_Fighting : AI_Node
{
    public Node_Fighting(int agression, int friendliness, int greediness) :
        base(agression, friendliness, greediness)
    {

    }

    public override int CarFighting()
    {
        
        return 1;
    }
}
