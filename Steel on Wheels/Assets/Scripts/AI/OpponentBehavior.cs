﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentBehavior : MonoBehaviour
{
    public Vehicle vehicle;
    public AI_Node nodes;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //vehicle.ControlAccelerate();
        switch (nodes.active_node)
        {
            case 0: //Apenas correndo
                nodes.active_node = nodes.CarRunning();
                break;
            case 1: //Apenas lutando
                nodes.active_node = nodes.CarFighting();
                break;
            case 2: //Lutando e correndo simultaneamente
                nodes.active_node = nodes.CarMixed();
                break;
            case 3: //Ajudando outro jogador
                nodes.active_node = nodes.CarHelping();
                break;
        }
    }
}
