﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AI_Node : MonoBehaviour
{
    [Range(0, 3)] public int active_node = 0; //Nodo atualmente ativado
    protected RaycastHit[] vehicle_eyes = new RaycastHit[8]; //O jeito que o veículo vê em volta de si
    public Vehicle vehicle_script; //Script que cuida do veículo
    protected System.Random rng = new System.Random();

    [Header("Behaviour Parameters")]
    [Range(1, 10)] public int aggression; //Agressividade do carro
    [Range(1, 10)] public int friendliness; //Companherismo do carro
    [Range(1, 10)] public int greediness; //Ganância do carro

    public AI_Node(int agression, int friendliness, int greediness)
    {
        this.aggression = agression;
        this.friendliness = friendliness;
        this.greediness = greediness;
    }

    public virtual int CarRunning() //Se o carro está apenas correndo
    {
        return active_node;
    }

    public virtual int CarFighting() //Se o carro está apenas lutando
    {
        return active_node;
    }

    public virtual int CarMixed() //Se o carro está correndo e lutando
    {
        return active_node;
    }

    public virtual int CarHelping() //Se o carro está ajudando
    {
        return active_node;
    }
}
