﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node_Running : AI_Node
{
    public Node_Running(int agression, int friendliness, int greediness) :
        base(agression, friendliness, greediness)
    {

    }

    public override int CarRunning()
    {
        for (int i = 0; i < vehicle_eyes.Length; i++)
        {
            if ((Physics.Raycast(vehicle_script.gameObject.transform.position, transform.TransformDirection(((i - 8) * 4.5f), 0, 1), out vehicle_eyes[i], 30))
                 && (string.Equals(vehicle_eyes[i].collider.gameObject.tag, "Vehicle")))
            {
                Debug.DrawRay(vehicle_script.gameObject.transform.position, transform.TransformDirection(((i - 8) * 4.5f), 0, 1) * vehicle_eyes[i].distance, Color.yellow);
                if ((rng.Next(0, 11) <= aggression)/* || 
                    ((vehicle_eyes[i].collider.gameObject.GetComponent<Vehicle>().) && ())*/) //Baseado na agressão, aleatoriamente causa o oponenete a trocar de comportamento para um de ataque
                {
                    return 2;
                } else if ((rng.Next(0, 11) <= friendliness)/* &&
                    (vehicle_eyes[i].collider.gameObject)*/)
                {
                    return 3;
                }
            }
            else
            {
                Debug.DrawRay(vehicle_script.gameObject.transform.position, transform.TransformDirection(((i - 8) * 4.5f), 0, 1) * vehicle_eyes[i].distance, Color.white);
            }
        }
        return 0;
    }
}
