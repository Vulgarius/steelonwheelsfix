﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class RandomColor : MonoBehaviour
{
    public Material[] material;
    public GameObject cubo;

    private float _timeBetweenColor = 1;

    private int _randomLights;
    private float _randomLightTime; 
    void Start()
    {
       
    }

    // Update is called once per frame
    IEnumerator TypeOfColor()
    {
        _randomLights = Random.Range(0, material.Length);
        _randomLightTime= Random.Range(0, _timeBetweenColor);

        for (int i = 0; i < material.Length; i++)
        {
            yield return new WaitForSeconds( _randomLightTime);
            Material mat =  new Material(material[i]);
           // Color color = new Color(_randomLights, _randomLights,_randomLights);
            cubo.GetComponent<Renderer>().material = mat;
        }
    }

    private void Update()
    {
        StartCoroutine(TypeOfColor());
    }
}
