﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sphereCool : MonoBehaviour
{
    private Carro_IA carro;
    void Start()
    {
        carro = GameObject.FindGameObjectWithTag("Vehicle2").GetComponent<Carro_IA>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
       if(other.CompareTag("Vehicle"))
       {
           carro.atual = 0;
           carro.currenState = Carro_IA.States.Attack;
           //Debug.Log("ATAQUE FOLLOW");
       }else if (other.CompareTag("Arena"))
       {
           carro.atual = 0;
           carro.currenState = Carro_IA.States.Arena;
       }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Vehicle"))
        {
            carro.currenState = Carro_IA.States.Arena;
            //Debug.Log("ARENA RANDOM");
        }
      
    }
}
